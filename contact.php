<?php

require_once("dbCore.php");

/**
 * Class Contact для сопоставления контактов
 *
 */
class Contact
{
    private $_db;
    private $_sqlConnect;
    private $_data = [];
    private $_checkDuplicate;

    function __construct()
    {
        $this->_db = dbCore::getInstance();
        $this->_sqlConnect = $this->_db->getConnection();
    }
    /**
     *Получение контактов и отправка их на обработку
     */

    /*
     * P.S.! для исключения всякой ереси типа контакта с одним именем
     * проверка кол-ва частей в поле
     * P.P.S. ну вдруг непонятно
     */
    public function getContacts()
    {
        $querySql = "SELECT "
                  . "users_tmp.id AS id, users_tmp.name AS fio,"
                  . "users_tmp.email AS email, "
                  . "users_tmp.email_dop AS dop_email, users_tmp.kodOU AS job "
                  . "FROM users_tmp "
                  . "WHERE email != '0' AND email != ''";
        foreach ($this->_sqlConnect->query($querySql) as $row) {
            $this->_checkDuplicate = false;
            $fio = preg_replace("/(\s){2,}/",' ', trim($row['fio']));
            if (count(explode(" ", $fio)) < 3) {
                continue;
            }
            $this->_compareEmail($fio, $row['email'], $row['id']);
            if (!empty($row['dop_email'])) {
                $this->_compareEmail($fio, $row['dop_email'], $row['id']);
            }
            /*
            if (!empty($row['job'])) {
                $this->_compareJob($fio, $row['job'], $row['id']);
            }
            */
            if ($this->_checkDuplicate) {
                $this->_insertDuplicate($this->_data['id'], $row['id']);
            } elseif (array_key_exists('id', $this->_data)) {
                $this->_insertId($row['id']);
            }
            $this->_data = [];
        }
        unset($this->_data);
    }
    /**
     * Сравнение контактов по email'у
     *
     * @param $userName
     * @param $userEmail
     * @param $userId
     */

    /*
     * P.S. в каких-то контактах фио имеет вид:
     *  "- ф и о"
     * или "ф и о -должность"
     * и т.д.
     */
    private function _compareEmail($userName, $userEmail, $userId)
    {
        $querySql = "SELECT "
                  . "contact.id AS crm_id,"
                  . "contact_email.email AS email "
                  . "FROM contact INNER JOIN contact_email "
                  . "ON (contact.id = contact_email.contact_id) "
                  . "WHERE contact.fio LIKE '%$userName%' AND contact.is_del = 0";
        $results = $this->_sqlConnect->query($querySql);
        if ($results) foreach ($results as $row) {
            if ($userEmail == $row['email'] && !empty($row['email'])) {
                $this->_pushData($row['crm_id'], $userId);
            }
        }
    }
    /*
     * Сравнение контактов по месту работы
     */

    /*
     * P.S. Т.к. в crm мб не указан email - поэтому like
     */
    private function _compareJob($userName, $userJob, $userId)
    {
        $querySql = "SELECT "
                  . "contact.id AS id, contact.place_job AS job "
                  . "FROM contact "
                  . "WHERE contact.fio like '%$userName%' "
                  . "AND contact.place_job IS NOT NULL "
                  . "AND contact.place_job != '' "
                  . "AND contact.is_del = 0";
        $results = $this->_sqlConnect->query($querySql);
        if ($results) foreach ($results as $row) {
            if ($userJob == $row['job']) {
                $this->_pushData($row['id'], $userId);
            }
        }
    }
    /**
     * Обработка полученной информации
     * Все дубликаты записываются в таблицу, кроме первого найденного
     * @param $crmId
     * @param $userId
     */
    private function _pushData($crmId, $userId)
    {
        if (!array_key_exists('id', $this->_data)) {
            $this->_data['id'] = $crmId;
        } elseif ($this->_data['id'] != $crmId) {
            $this->_checkDuplicate = true;
            $this->_insertDuplicate($crmId, $userId);
        }
    }

    /**
     * Вставка в таблицу дубликатов
     * @param $crmId
     * @param $userId
     */
    private function _insertDuplicate($crmId, $userId)
    {
        $querySql = "INSERT into duplicates (crm_id, best_id) "
                  . "VALUES ($crmId, $userId)";
        $this->_sqlConnect->query($querySql);
    }

    /**
     * Вставка crmid для контакта из bestedu
     * @param $userId
     */
    private function _insertId($userId)
    {
        $crmId = $this->_data['id'];
        $querySql = "UPDATE users_tmp "
                  . "SET contact_crm_id = $crmId "
                  . "WHERE id = $userId";
        $this->_sqlConnect->query($querySql);
    }
}

