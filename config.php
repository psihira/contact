<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22.08.2017
 * Time: 11:29
 */

class Config
{
    static $confArray;

    public static function read($name)
    {
        return self::$confArray[$name];
    }

    public static function write($name, $value)
    {
        self::$confArray[$name] = $value;
    }

}


Config::write('db.host', '127.0.0.1');
Config::write('db.basename', 'contacts');
Config::write('db.user', 'root');
Config::write('db.password', '');

